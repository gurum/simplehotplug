/*
 * SimpleHotplug.cc
 *
 *  Created on: May 18, 2016
 *      Author: buttonfly
 *
 *      I highly recommend you to use udev.
 */

#include "simplehotplug/Hotplug.h"

#include <string>
#include <ctype.h>
#include <sys/un.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/types.h>
#include <linux/netlink.h>
#include <unistd.h>
#include <sys/poll.h>
#include <fcntl.h>
#include <mntent.h>
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <errno.h>
#include <iostream>
#include <sys/stat.h>

using namespace std;

namespace gurum {

Hotplug::Hotplug() : _stopped(false), _started(false), _sck(-1), _proc(-1), _numofmounted(0) {
	pthread_mutexattr_t mutexAttr;
	pthread_mutexattr_init(&mutexAttr);

	pthread_mutexattr_settype(&mutexAttr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutexattr_settype(&mutexAttr, PTHREAD_MUTEX_FAST_NP);

	pthread_mutex_init(&_lck, &mutexAttr);
	pthread_mutexattr_destroy(&mutexAttr);
}

Hotplug::~Hotplug() {
	if(! _stopped) stop();

	pthread_mutex_destroy(&_lck);
}

void Hotplug::addDelegate(gurum::HotplugDelegate *delegate) {
	lock();
	_delegates.push_back(delegate);
	unlock();
}

void Hotplug::removeDelegate(gurum::HotplugDelegate *delegate) {
	lock();
	_delegates.remove(delegate);
	unlock();
}

void Hotplug::firePlugged(std::string &msg) {
	lock();
	for(auto pos = _delegates.begin(); pos != _delegates.end();++pos) {
		(*pos)->plugged(msg);
	}
	unlock();
}

void Hotplug::fireUnplugged(std::string &msg) {
	lock();
	for(auto pos = _delegates.begin(); pos != _delegates.end();++pos) {
		(*pos)->unplugged(msg);
	}
	unlock();
}

void Hotplug::fireMounted(std::string &fsname, std::string &mnt) {
	lock();
	for(auto pos = _delegates.begin(); pos != _delegates.end();++pos) {
		(*pos)->mounted(fsname, mnt);
	}
	unlock();
}

void Hotplug::fireUnmounted(std::string &fsname, std::string &mnt) {
	lock();
	for(auto pos = _delegates.begin(); pos != _delegates.end();++pos) {
		(*pos)->unmounted(fsname, mnt);
	}
	unlock();
}

int Hotplug::start() {
	if(_started) {
		fprintf(stderr, "W: It's already started.\n");
		return 0;
	}

	_sck = nlopen();
	if(_sck < 0) {
		return -1;
	}

	_proc = procopen();
	if(_proc < 0) {
		return -1;
	}

    pthread_attr_t        attribute;
    pthread_attr_init(&attribute);
    pthread_attr_setscope(&attribute, PTHREAD_SCOPE_SYSTEM);
    pthread_attr_setdetachstate(&attribute, PTHREAD_CREATE_JOINABLE);
	pthread_create(&_thread, &attribute, (void* (*)(void*)) thr, (void*)this);
	pthread_attr_destroy(&attribute);

	updateMount();
	_started = true;
	return 0;
}

void Hotplug::stop() {
	if(_thread == pthread_self()) {
		fprintf(stderr, "W: The dispatcher thread is not allowed to control this instance\n");
		return;
	}

	if(_stopped) return;

	_stopped = true;

	int status;
	pthread_join(_thread, (void **)&status);

	if(_sck > 0)
		close(_sck);
	_sck = -1;

	if(_proc > 0)
		close(_proc);
	_proc = -1;
}

void Hotplug::thr(void *arg) {
	Hotplug *self = (Hotplug *) arg;

	int clntsck = self->_sck;
	int clntproc = self->_proc;
	for(; ! self->_stopped;) {
		self->waitUevent(clntsck, 5);
		self->waitMountEvent(clntproc, 5);
	}
}

int Hotplug::nlopen() {
	int sck = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_KOBJECT_UEVENT);
	if(sck < 0) {
		fprintf(stderr, "E: failed to open netlink.\n");
		return sck;
	}

	//	setsockopt(_nl_sck, SOL_SOCKET, SO_RCVBUFFORCE);

	 int val = 1;
	setsockopt(sck, SOL_SOCKET, SO_REUSEADDR, (char *) &val, sizeof(val));

	struct sockaddr_nl nl;
	memset(&nl, 0, sizeof(struct sockaddr_nl));
	nl.nl_family = AF_NETLINK;
	nl.nl_pid = getpid();
	nl.nl_groups = -1;
	int r = bind(sck, (const struct sockaddr*) &nl, sizeof(struct sockaddr_nl));
	if(r < 0) {
		fprintf(stderr, "E: failed to bind -> nlopen\n");
		goto exception;
	}
	return sck;

exception:
	close(sck);
	return -1;
}

int Hotplug::procopen() {
	int fd = open("/proc/mounts", O_RDONLY, 0);
	if(fd < 0) {
		fprintf(stderr, "E: failed to open /proc/mounts\n");
		return -1;
	}
	return fd;
}

void Hotplug::waitMountEvent(int clnt, int timeout) {
    struct pollfd p;

    p.fd = clnt;
    p.events = POLLERR | POLLPRI;
    p.revents = 0;
    poll(&p, 1, timeout);
	if (p.revents & POLLERR) {
		// changed
		// fprintf(stderr, "mount changed\n");
		updateMount();
	}
}

void Hotplug::waitUevent(int clnt, int timeout) {
	struct iovec iov;
	char buf[4096];
	memset(buf, 0, sizeof(buf));
	iov.iov_base = &buf;
	iov.iov_len = sizeof(buf);

	struct msghdr hdr;
	struct sockaddr_nl nladdr;
	memset(&hdr, 0, sizeof(struct msghdr));
	hdr.msg_iov = &iov;
	hdr.msg_iovlen = 1;
	hdr.msg_name = &nladdr;
	hdr.msg_namelen = sizeof(nladdr);
	hdr.msg_control = NULL;
	hdr.msg_controllen = 0;
	hdr.msg_flags = 0;


    int ret = -1;
    int rc = -1;
    // int timeout = 500;
    fd_set readfds;
    struct timeval to = { 0 };
    struct timeval *p_to = NULL;

    FD_ZERO(&readfds);
    FD_SET(clnt, &readfds);

    if (0 != timeout) {
            to.tv_sec = timeout / 1000;
            to.tv_usec = (timeout % 1000) * 1000;
            p_to = &to;
    }

   	do
    	rc = select(clnt + 1, &readfds, NULL, NULL, p_to);
   	while (EINTR == errno);
    if (rc == -1) {
    	// error
    }
    else if (rc > 0) {
    	int  len = recvmsg(clnt, &hdr, 0);
    	buf[len] = 0;

    	string msg = buf;
    	// cerr << msg << endl;


//    	auto at = msg.find("block");
//        if(at == std::string::npos) return;
//
//    	// ex) add@/block/sda/sda1
//    	// ex) remove@/block/sda/sda1
    	// ex) change@...
//    	// We will focus on the remove event.
		auto at = msg.find("@");
		if(at == std::string::npos) return;

		string ev = msg.substr(0, at);
		string value = msg.substr(at + 1);
		if(ev.compare("add")==0) {
			firePlugged(value);
		}
		else if(ev.compare("remove")==0) {
			fireUnplugged(value);
		}
		else {
			// I don't care.
			fprintf(stderr, "%s\n", msg.c_str());
		}

    	// everytime AP is changed.
    	// /devices/platform/regulatory.0

    	// enable / disable wlan0
//    	change@/devices/pci0000:00/0000:00:1c.1/0000:04:00.0/ieee80211/phy0/rfkill1
//    	libudev
//    	change@/devices/pci0000:00/0000:00:1c.1/0000:04:00.0/net/wlan0/rfkill2



    	// disk remove
//    	remove@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host12/target12:0:0/12:0:0:0/bsg/12:0:0:0
//    	remove@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host12/target12:0:0/12:0:0:0/scsi_generic/sg1
//    	remove@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host12/target12:0:0/12:0:0:0/scsi_device/12:0:0:0
//    	remove@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host12/target12:0:0/12:0:0:0/scsi_disk/12:0:0:0
//    	libudev
//    	libudev
//    	libudev
//    	libudev
//    	remove@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host12/target12:0:0/12:0:0:0/block/sdb/sdb1
//    	remove@/devices/virtual/bdi/8:16
//    	remove@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host12/target12:0:0/12:0:0:0/block/sdb
//    	remove@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host12/target12:0:0/12:0:0:0
//    	libudev
//    	remove@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host12/target12:0:0/12:0:0:1/bsg/12:0:0:1
//    	remove@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host12/target12:0:0/12:0:0:1/scsi_generic/sg2
//    	remove@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host12/target12:0:0/12:0:0:1/scsi_device/12:0:0:1
//    	remove@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host12/target12:0:0/12:0:0:1
//    	remove@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host12/target12:0:0
//    	remove@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host12/scsi_host/host12
//    	remove@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host12
//    	remove@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0
//    	libudev
//    	remove@/devices/pci0000:00/0000:00:14.0/usb3/3-2
//    	libudev

    	// disk add
//    	add@/devices/pci0000:00/0000:00:14.0/usb3/3-2
//    	add@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0
//    	add@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host13
//    	add@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host13/scsi_host/host13
//    	libudev
//    	libudev
//    	libudev
//    	libudev
//    	add@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host13/target13:0:0
//    	add@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host13/target13:0:0/13:0:0:0
//    	add@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host13/target13:0:0/13:0:0:0/scsi_disk/13:0:0:0
//    	add@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host13/target13:0:0/13:0:0:0/scsi_device/13:0:0:0
//    	add@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host13/target13:0:0/13:0:0:0/scsi_generic/sg1
//    	add@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host13/target13:0:0/13:0:0:0/bsg/13:0:0:0
//    	add@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host13/target13:0:0/13:0:0:1
//    	add@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host13/target13:0:0/13:0:0:1/scsi_device/13:0:0:1
//    	add@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host13/target13:0:0/13:0:0:1/scsi_generic/sg2
//    	libudev
//    	add@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host13/target13:0:0/13:0:0:1/bsg/13:0:0:1
//    	add@/devices/virtual/bdi/8:16
//    	libudev
//    	add@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host13/target13:0:0/13:0:0:0/block/sdb
//    	add@/devices/pci0000:00/0000:00:14.0/usb3/3-2/3-2:1.0/host13/target13:0:0/13:0:0:0/block/sdb/sdb1


    }
    /* Else it timed out */
}

//@deprecated
#if 0
void Hotplug::run(int clnt) {
	if(clnt < 0) return;

	waitUevent(clnt, 500);
}
#endif

void Hotplug::lock() {
	pthread_mutex_lock(&_lck);
}

void Hotplug::unlock() {
	pthread_mutex_unlock(&_lck);
}

void Hotplug::updateMount() {
	
	std::map<std::string, std::string> mntlst;
	struct mntent *ent;
  	FILE *fp;

  	fp = setmntent("/proc/mounts", "r");
  	if (fp == NULL) {
  		fprintf(stderr, "failed to open /proc/mounts\n");
  		return;
  	}

  	for (; NULL != (ent = getmntent(fp));) {
    	// printf("%s %s\n", ent->mnt_fsname, ent->mnt_dir);
    	mntlst[ent->mnt_dir] = ent->mnt_fsname;
  	}

	endmntent(fp);
	fp = NULL;

	int numofmounted = mntlst.size();

	if(_numofmounted > numofmounted) {
  		// removed
  		for(auto &pos : _mntlst) {
  			auto it = mntlst.find(pos.first);
  			if(it == mntlst.end()) {
  				// std::cout << "[" << pos.first << ":" << _mntlst[pos.first] 
  				// << " has been removed" << "]" << std::endl;
				std::string mnt = pos.first;
				std::string fsname = _mntlst[pos.first];
				fireUnmounted(fsname, mnt);
				// break;
  			}
  		}
	}
	else { // It can't be same. _numofmounted > i
  		// added
  		for(auto &pos : mntlst) {
  			auto it = _mntlst.find(pos.first);
  			if(it == _mntlst.end()) {
  				// std::cout << "[" << pos.first << ":" << mntlst[pos.first].c_str() 
  				// << " has been added" << "]" << std::endl;
				std::string mnt = pos.first;
				std::string fsname = mntlst[pos.first];
  				fireMounted(fsname, mnt);
  				// break;
  			}
  		}
	}

	_numofmounted = numofmounted;
	_mntlst = mntlst;
}

}
