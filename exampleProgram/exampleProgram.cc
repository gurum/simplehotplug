/*
 ============================================================================
 Name        : exampleProgram.c
 Author      : buttonfly
 Version     :
 Copyright   : Your copyright notice
 Description : Uses shared library to print greeting
               To run the resulting executable the LD_LIBRARY_PATH must be
               set to ${project_loc}/libsimplehotplug/.libs
               Alternatively, libtool creates a wrapper shell script in the
               build directory of this program which can be used to run it.
               Here the script will be called exampleProgram.
 ============================================================================
 */

#include "simplehotplug/Hotplug.h"
#include <string>
#include <iostream>

#ifndef OVERRIDE
#define OVERRIDE
#endif


using namespace gurum;
using namespace std;

class LocalDelegate: public HotplugDelegate {
public:
	LocalDelegate() {

	}

	virtual ~LocalDelegate() {

	}

	void plugged(std::string &msg) {
		std::cout << "plugged: " << msg << std::endl;
	}

	void unplugged(std::string &msg) {
		std::cout << "unplugged: " << msg << std::endl;
	}

	void mounted(std::string &fsname, std::string &mnt) {
		std::cout << "mounted: " << fsname << ":" << mnt << std::endl;
	}

	void unmounted(std::string &fsname, std::string &mnt) {
		std::cout << "unmounted: " << fsname << ":" << mnt << std::endl;
	}

};

int main(void) {

	Hotplug hotplug;
	hotplug.addDelegate(new LocalDelegate);
	hotplug.start();

	string c;
	getline(cin, c);

	hotplug.stop();
	return 0;
}
