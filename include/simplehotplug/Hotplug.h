/*
 * Hotplug.h
 *
 *  Created on: May 18, 2016
 *      Author: buttonfly
 */

#ifndef GURUM_HOTPLUG_H_
#define GURUM_HOTPLUG_H_

#include <string>
#include <pthread.h>
#include <list>
#include <map>

#ifndef OVERRIDE
#define OVERRIDE
#endif

namespace gurum {

class HotplugDelegate {
public:
	virtual void plugged(std::string &msg)=0;
	virtual void unplugged(std::string &msg)=0;

	virtual void mounted(std::string &fsname, std::string &mnt)=0;
	virtual void unmounted(std::string &fsname, std::string &mnt)=0;
};

class Hotplug {
public:
	Hotplug();
	virtual ~Hotplug();

	int start();
	void stop();
	void addDelegate(gurum::HotplugDelegate *delegate);
	void removeDelegate(gurum::HotplugDelegate *delegate);


private:
	static void thr(void *arg);
	void waitUevent(int clnt, int timeout);
	void waitMountEvent(int clnt, int timeout);
	//@deprecated
#if 0
	void run(int clnt);
#endif
	int nlopen();
	int procopen();
	void firePlugged(std::string &msg);
	void fireUnplugged(std::string &msg);
	void fireMounted(std::string &fsname, std::string &mnt);
	void fireUnmounted(std::string &fsname, std::string &mnt);

	void lock();
	void unlock();

	void updateMount();

private:
	fd_set _readfds;
	int _fd_max;
	pthread_t _thread;
	pthread_mutex_t _lck;

	bool _stopped;
	bool _started;
	int _sck;
	int _proc;
	std::list<gurum::HotplugDelegate *> _delegates;
	int _numofmounted;
	std::map<std::string, std::string> _mntlst;
};

} /* namespace gurum */

#endif /* GURUM_HOTPLUG_H_ */
